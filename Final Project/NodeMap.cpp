#include "NodeMap.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : NodeMap.h
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// Nodes are organized in a structure relative to the needs of the structure and can be located by traversing the
// structure. However, nodes can have other properties that may be used to identify them more quickly. This map 
// is used to facilitate a quicker lookup. The property this map uses is the attribute "id" (case sensitive). That
// is, NodeMap facilitates finding a reference to a node based on the value of the attribute "id" rather than 
// a position in a structure.
//
// A NodeMap implements the INodeListener interface to ensure that its map is properly sorted whenever changes to 
// the nodes occur.
//
// Known Limitations
// The map is implemented to only support sorting based on the attribute "id". Future versions might provide the
// ability to sort by other attributes.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



NodeMap::NodeMapNode::NodeMapNode(Node *initNode, NodeMapNode* initLeft /*= 0*/, NodeMapNode* initRight /*= 0*/)
{
	node  = initNode;
	left  = initLeft;
	right = initRight;
}

void NodeMap::informIdRemoved(const string &oldValue)
{
	deleteNode(oldValue, root);
}

NodeMap::~NodeMap()
{
	//Delete both sides of the tree.
	while(root->right!=0 || root->left!=0)
	{
		delete(root->right);
		delete(root->left);
	}
}
//USE RECURSION
void NodeMap::deleteNode(const string &id, NodeMapNode *&n)
{
	//Recursively loop through all the nodes until the ID matches.
	//Make sure to check that the root isn't null, though.
	if(n!=0)
	{
	if (n->node->getId().compare(id)==0)
	{
		informNodeDestroyed(n->node);
		delete n->node;
	}
	else
	{
		deleteNode(id, n->right);
		deleteNode(id,n->left);
	}
	}
}

NodeMap::NodeMap()
{
	root =0;
}

void NodeMap::add(Node* node)
{
	//Loop through the nodes while there are still nodes remaining.
	//Check the id then do a binary search to focus where to go
	//Then, add the node in that position.
	NodeMapNode* temp = root;
	if(root==0)
		temp->node = node;
	else
	{
		while(temp!=0)
		{		
		if(node->getId().compare(root->node->getId())<0)
		{
			if(temp->left==0)
				temp->left->node = node;
			else
				temp = temp->left;
		}
		else
		{
			if(temp->right==0)
				temp->right->node = node;
			else
				temp = temp->right;
		}
		}
	}
	root = temp;
}

void NodeMap::remove(const string &id)
{
	deleteNode(id, root);
}

Node* NodeMap::findById(const string &id)
{
	//Loop through the nodes while there are still nodes remaining.
	//Check the id then do a binary search to focus where to go
	NodeMapNode* temp = root;
	if(root==0)
		return 0;
	else
	{
		while(temp!=0)
		{		
			if(temp->node->getId().compare(id)<0)
			{
				if(temp->left!=0)
					temp = temp->left;
			}
			else if(temp->node->getId().compare(id)>0)
			{
				if(temp->right!=0)
					temp= temp->right;
			}
			else
				return temp->node;
		}
	}
	root = temp;
}
void NodeMap::informIdChanged(Node *node, const string &oldId)
{
	//Delete the node then add it again so it's balanced.
	deleteNode(oldId,root);
	add(node);
}

void NodeMap::informNodeDestroyed(Node* node)
{
	deleteNode(node->getId(),root);
}