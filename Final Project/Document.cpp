#include "Document.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : Document.cpp
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// Document represents the primary entity for the model of the document structure. It is the context in which
// Element objects reside.
//
// A document is represented by a tree structure. If a document has content, it will have a unique root element
// which is entry point into the document structure. The document provides a factory method for creating, finding,
// and accessing elements.
//
// A document may have elements that are not part of the document structure. When elements are created they are in 
// the document context but not in the document structure.
// 
//
// Known Limitations
// Ideally, documents should not be able to share elements. That is, when an element is created in one 
// document context it cannot be appended to an element in another document context. This is currently not
// part of the implementation. It's up to the user to ensure elements are not moved between contexts. Doing so will
// lead to a variety of conflicts.
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////




bool Document::isDocument()
{
	//As defined by the header
	return true;
}

Element* Document::removeRoot()
{
	RealElement* temp = root;
	delete root;
	root = 0;
	return temp;
}

void Document::setRoot(Element* newRoot)
{
	//Use dynamic cast to turn into RealElement.
	root = dynamic_cast<RealElement*>(newRoot);

}

Element* Document::getRoot()
{
	return root;
}

Element* Document::findElementById(const string &id)
{
	RealElementNode* temp = elementsHead;
	while(temp->next!=0)
	{
		if(temp->element->getName().compare(id)==0)
		{
			return temp->element;
		}
		temp=temp->next;
	}
	return;
}

Element* Document::createElement(const string &name)
{
	RealElement* myElement = new RealElement(name);
	myElement->setName(name);
	return myElement;
}

Document::~Document()
{
	delete root;
	delete elementsHead;
	delete idTree;
}

Document::Document()
{
	root = 0;
	elementsHead =0;
	idTree =0;
}

Document::RealElementNode::RealElementNode(RealElement *initRealElement, RealElementNode* initNext /*= 0*/)
{
	element = initRealElement;
	next    = initNext;
}
//It was said to not modify without consulting Dr. Smith.
Document::RealElement::~RealElement()
{

}

Document::RealElement::RealElement(const string &initName) : Element(initName)
{
	setName(initName);
}