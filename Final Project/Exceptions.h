///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : Exceptions.h
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// This file contains the declaration of a variety of exceptions that can be thrown within the system. The reason
// a particular exception is thrown is explain the description of operations that throw them.
//
// Every exception defined in the system should be a subclass of Exception to promote robust exception handling.
// 
// All exceptions should be implemented inline (there should be no associated .cpp files).
//
// Known Limitations
// None.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>

using namespace std;

// This class is the base class for all exceptions in the system.
//
class Exception {
private:
    
    // The message containing further details about the error. There are no constraints.
    //
    string message;

public:
 

    // This constructor initializes the message to "No further details."
    //
    Exception() {
        message = "No further details.";
    }


    // These operations set and get the message, respectively.
    //
    void setMessage(string &newMessage) {
        message = newMessage;
    }

    string getMessage() {
        return message;
    }

};

//This is thrown if n is negative or greater than the number of children.
class IndexOutOfBoundsException : public Exception
{
    try
    {
        ElementNode* temp = elementHead;
        for(int i=0;i<n-1;++i)
        {
            temp = temp->next;
        }
        ElementNode* temp2 = temp->next;
        temp->next = new ElementNode(newElement,temp2);
        elementHead = temp;
    }
    catch (n<0)
    {
        cerr << "n value cannot be negative." << endl;
    }
    catch (n>getChildCount)
    {
        cerr << "n value cannot be greater than the number of children."
    }
};


//This is thrown when the element’s id doesn't exist, is empty, or is all whitespace.
class InvalidFormatException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
};

// This is thrown if the element already has a parent. If an element is to be moved
// it must first be removed from its current parent.
class HasParentException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
};

// This is thrown for two reasons. If old id is empty (or all whitespace)
// and the node trying to be added is already in the listener’s
// collection or if the old id has non-whitespace characters but the
// node associated to the id is not that provided.
class DuplicateNodeException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
    
};


class DuplicateAttributeException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
};

// This exception is thrown if there is no attribute "id".
class MissingIdException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
    
};

// The document reference is either null or references a document with no
// root element. If this exception is thrown, the state of the file remains the
// same. This means if the file did not exist before the call it will not be
// created and if it did exist the file's contents will remain unchanged.
class EmptyDocumentException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
};

// This is thrown if the document already has a root. To establish a new root
// when one already exists, the root must first be removed.
class ExistingRootException : public Exception
{
    try
    {
        
    }
    catch ()
    {
        
    }
    
};
        
// This exception is thrown when there is a problem writing to the file specified
// by the file path. This could occur for multiple reasons. See the exception
// message for details. If this exception is thrown the state of the specified file
// is not guaranteed.
class FileException : public Exception
{
    try
    {
        file.open ("test.xml");
        while (!file.eof()) file.get();
        file.close();
    }
    catch (ifstream::failure e)
    {
        cerr << "Exception opening, reading, or closing file";
    }
    
};


#endif
