#include "DocumentImporter.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : DocumentImporter.h
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// This class provides utility operations for importing a document from different formats. Any format that serves
// as a source must represent a well-formed XML document.
//
// Known Limitations
// None.
//   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Document* DocumentImporter::importDocumentFromFile(const string &filepath)
{
		//No code implemented, but general process:
		//1. Parse through file looking for "<" and then make a new node with the following
		//String as the name of it (ex, "<Movie")
		//2.  Continue parsing until there is a "</" signifying the end of that section
		//And its children (ex, "</Movie")
		//3.  Continue this for all the lines in the file until it is done.
}
