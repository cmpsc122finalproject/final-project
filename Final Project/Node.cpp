#include "Node.h"
#include <iostream>
#include "NodeMap.h"
#include "INodeListener.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : Node.cpp
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// The class Node represents an entity in a structure. A node maintains an set attributes (name/value pairs) and
// a set of listeners interested in being informed of certain changes made to the node.
//
// Known Limitations
// None.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace std;

Node::AttributeNode::AttributeNode(string initName, string initValue, AttributeNode *initNext /*= 0*/)
{
	name  = initName;
	value = initValue;
	next  = initNext;
}

Node::INodeListenerNode::INodeListenerNode(INodeListener *initListener, INodeListenerNode *initNext /*= 0*/)
{
	listener = initListener;
	next     = initNext;
}

Node::Node()
{
	//Set the initial attributes head.
	attributesHead = new AttributeNode("","",0);
}

Node::~Node()
{
	delete attributesHead;
	//Notify listeners.

}

void Node::setAttribute(const string &name, const string &value)
{
	AttributeNode* temp = attributesHead;
	while(temp->next!=0)
	{
		if(temp->name.compare(name)==0)
		{
			temp->value = value;
			attributesHead = temp;
			break;
		}
	}
}

//ASSUMING THIS METHOD IS MEANT TO REMOVE THE ENTRY FOR AN ATTRIBUTE WITH THAT NAME.
//IF THIS IS JUST TO REMOVE THE VALUE, THEN REVERSE THE SETATTRIBUTE.
void Node::removeAttribute(const string &name)
{
	//Get a temporary list to search through until the "next" is a null pointer.
	AttributeNode* temp = attributesHead;
	while(temp->next!=0)
	{
		//If the name equals what is given, then bypass the next by skipping over it, 
		//then delete the next.
		if(temp->next->name.compare(name)==0)
		{
			AttributeNode* temp2 = temp->next;
			temp->next = temp2->next;
			attributesHead = temp;
			delete &temp2;
		}
		else
			temp = temp->next;
	}
}
//TODO: Exception if name is not found.
std::string Node::getAttribute(string name)
{
	AttributeNode* temp = attributesHead;
	while(temp->next!=0)
	{
		if(temp->name.compare(name)==0)
		{
			return temp->value;
		}
	}
}

std::string Node::getAttributeNameAt(int position)
{
	//Verify the position
	//TODO: Add the end verification.
	if(position>=0)
	{
		//Do a temporary list and make a bool to see i
	AttributeNode* temp = attributesHead;
	for(int i=0;i<position;i++)
	{
		temp = temp->next;
	}
	return temp->name;
	}
	else
		return "";
}

std::string Node::getId()
{
	cout << getAttribute("id");
}

void Node::addIdListener(INodeListener* listener)
{
	//Add validation;
	INodeListenerNode* temp = listenersHead;
	int i=0;
	while(temp->next->listener!=0)
	{
		i++;
	};
	NodeMap *myNode = new NodeMap();
	INodeListener* myListener =myNode;
	for(int j=0;j<i-1;j++)
	{
		myListener[j] = listenersHead->listener[j];
	}
	myListener[i] = listener[0];
	listenersHead->listener = myListener;
	delete temp;


}

void Node::removeIdListener(INodeListener* listener)
{
	//Add validation;
	INodeListenerNode* temp = listenersHead;
	int i=0;
	int j=0;
	//Have 2 ints that track the index of the one to remove and the total number of items.
	//Then make a nodemap with size j-1, as we will be removing 1 object.
	//After that, we loop through until we get to the one we're removing.
	//We then switch to the other loop and skip over that value for listener[i], effectively removing it
	//Then we set it back to the class variable value and we're good to go.
	while(temp->next->listener!=0)
	{
		if(temp->next->listener!=listener)
		{
			++i;
		}
		++j;
	}
	NodeMap *myNode = new NodeMap();
	INodeListener* myListener = myNode;
	for(int k=0;k<i-1;k++)
	{
		myListener[k] = temp->listener[k];
	}
	for(int k=i;k<j-2;k++)
	{
		myListener[k] = temp->listener[k+1];
	}
	listenersHead->listener = myListener;
	delete temp;
}
