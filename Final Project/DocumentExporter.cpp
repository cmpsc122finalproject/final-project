#include "DocumentExporter.h"
#include "Element.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : DocumentExporter.cpp
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// This class provides utility operations for exporting a document to other formats.
//
// Known Limitations
// The content of a document represents and XML document. When writing the contents to a file or console, the 
// output will be written in a human-readable format by indenting child elements. So, if the document consists
// of a root element 'a' with a single child 'b' that has text "This is content." the output will look
// 
// <a>
//     <b>This is content.</b>
// </a>
//
// The indentation is always four spaces. In the future it will be useful to define the number of spaces in the 
// indentation and/or indicate the content does not have to be human-readable and the output is a single string
// with no line feeds.
//   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void DocumentExporter::exportDocumentToFile(Document* document, string filepath)
{
	string output = "";
	Element* myElement = document->getRoot();
}
