#include "INodeListener.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : INodeListener.cpp
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// This interface is used for receiving notifications when various aspects of a node changes.
//
// Known Limitations
// None.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void INodeListener::informIdChanged(Node* node, const string &oldIdValue)
{

}

void INodeListener::informNodeDestroyed(Node* node)
{

}

void INodeListener::informIdRemoved(const string &oldValue)
{

}
