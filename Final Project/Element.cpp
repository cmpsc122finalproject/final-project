#include "Element.h"
#include <string>
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// File Name : Element.cpp
//
// Author :  Matt Young
// UserID :  may5109
//
// Description
// The class Element represents an entity in the document structure. An element exists in the context of a document
// and may have a parent node if it is part of a larger structure and child elements. Elements always have a name
// and may optionally contain text.
//
// Known Limitations
// Elements are created within the context of a document and documents should not be able to share elements. 
// That is, when an element is created in one document context it cannot be appended to an element in another
// document context. This is currently not part of the implementation. It's up to the user to ensure elements are
// not moved between contexts. Doing so will lead to a variety of conflicts.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



Element::ElementNode::ElementNode(Element* initElement, ElementNode *initNext /*= 0*/)
{
	element = initElement;
	next    = initNext;
}

Element::Element(const string &initName)
{
	//TODO:
	//Check for whitespace
	if(initName.compare("")!=0)
	{
		childCount=0;
		name =initName;
	}
}

Element::~Element()
{
	delete elementHead;
}

bool Element::isDocument()
{
	//As per the header file.
	return false;
}

std::string Element::getName()
{
	return name;
}

void Element::setName(const string &newName)
{
	name = newName;
}

Node* Element::getParent()
{
	return parent;
}

void Element::setParent(Node *newParent)
{
	parent = newParent;
}

std::string Element::getText()
{
	return text;
}

void Element::setText(const string &newText)
{
	text = newText;
}

int Element::getChildCount()
{
	return childCount;
}

Element* Element::getAt(int n)
{
//Loop through all of the elements until you get the proper value.
	if((n<=getChildCount()-1) && n>=0)
	{
		ElementNode* temp = elementHead;
		for(int i=0;i<n;++i)
		{
			temp = temp->next;
		}
		return temp->element;
	}
	else
	{
		return;
	}
}

void Element::appendChild(Element* newElement)
{
//Insert at the end.
	childCount++;
	insertAt(childCount,newElement);
}

void Element::insertAt(int n, Element* newElement)
{
//Loop through the list and then set the new links.
	if(newElement!=0 && ((n<=getChildCount()) && n>=0))
	{
		ElementNode* temp = elementHead;
		for(int i=0;i<n-1;++i)
		{
			temp = temp->next;
		}
		ElementNode* temp2 = temp->next;
		temp->next = new ElementNode(newElement,temp2);
		elementHead = temp;
	}
}

Element* Element::removeChild(int n)
{
//Go to the index and then bypass that node by redoing the links.
	ElementNode* temp = elementHead;
	for(int i=0;i<n-1;++i)
	{
		temp = temp->next;
	}
	ElementNode* temp2 = temp->next->next;
	delete temp->next;
	temp->next = temp2;
	elementHead = temp;
}
